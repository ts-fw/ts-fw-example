import {Job} from 'ts-fw-agenda';
import * as winston from 'winston';
import {inject} from 'inversify';

@Job({interval: 'one minute'})
export class TestJob {
    @inject('logger')
    private logger: winston.Winston;

    async onExecute() {
        this.logger.info(`Current time: ${new Date().toISOString()}`);
    }
}
