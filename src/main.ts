import 'ts-fw';
import 'ts-fw-db';

import './configs';
import './controllers';
import './jobs';
import './services';
import './models';
import './fixtures';

import {ExpressServerApplication} from 'ts-fw';

const app: ExpressServerApplication = new ExpressServerApplication();
app.build();
