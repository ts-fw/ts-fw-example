import {Config, IConfigStorage} from 'ts-fw';
import {inject} from 'inversify';

@Config()
export class LocalStorageConfig {
    @inject('configStorage')
    private configStorage: IConfigStorage;

    config() {
        this.configStorage.load(require('../../config.json'));
    }
}
