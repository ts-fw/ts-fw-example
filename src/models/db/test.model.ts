import {BaseModel, Model} from 'ts-fw-db';
import {Column} from 'typeorm';

@Model()
export class Test extends BaseModel {
    @Column({unique: true})
    name: string;
}
