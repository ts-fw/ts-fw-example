import {HTTP} from 'ts-fw';
import {httpGet, httpDelete, httpPut, requestBody, response, requestParam} from 'inversify-express-utils';
import {inject} from 'inversify';
import {TestService} from '../services';
import {Test} from '../models/db';
import * as express from 'express';

@HTTP('/tests')
export class TestController {
    @inject('testService')
    private testService: TestService;

    @httpGet('/')
    getTestData(): Promise<Test[]> {
        return this.testService.getTestData();
    }

    @httpGet('/')
    async createTestData(@requestBody() instance: Test,
                         @response() res: express.Response): Promise<void> {
        await this.testService.createTestData(instance);
        res.status(201).json(instance);
    }

    @httpDelete('/:id([0-9]+)')
    async removeTestData(@requestParam('id') id: string): Promise<number> {
        await this.testService.removeTestData(id);
        return 200;
    }

    @httpPut('/:id([0-9]+)')
    async updateTestData(@requestParam('id') id: string,
                         @requestBody() instance: Test): Promise<Test> {
        await this.testService.updateTestData(id, instance);
        return instance;
    }

    @httpGet('/redis')
    getTestDataFromRedis(): Promise<Test> {
        return this.testService.getDataFromRedis();
    }
}
