import {lazyInject, Service} from 'ts-fw';
import {Test} from '../models/db';
import {Repository} from 'typeorm';
import {Redis} from 'ioredis';

@Service()
export class TestService {
    @lazyInject('testRepository')
    private testRepository: Repository<Test>;

    @lazyInject('redis')
    private redis: Redis;

    getTestData(): Promise<Test[]> {
        return this.testRepository.find();
    }

    createTestData(body: Test): Promise<Test> {
        return this.testRepository.persist(body);
    }

    async removeTestData(id: string): Promise<void> {
        const testData = await this.testRepository.findOneById(id);
        await this.testRepository.remove(testData);
    }

    async updateTestData(id: string, instance: Test): Promise<Test> {
        const testData = await this.testRepository.findOneById(id);
        Object.assign(testData, instance);
        await this.testRepository.persist(testData);
        return testData;
    }

    async getDataFromRedis(): Promise<Test> {
        return await this.redis.get('names');
    }
}
