import {Fixture} from 'ts-fw-db';
import {Test} from '../models/db';
import {lazyInject} from 'ts-fw';
import {Repository} from 'typeorm';
import {Redis} from 'ioredis';

@Fixture(Test)
export class AppFixture {
    @lazyInject('testRepository')
    private testRepository: Repository<Test>;

    @lazyInject('redis')
    private redis: Redis;

    async generate(): Promise<void> {
        const names: string[] = [
            'test data 1',
            'test data 2'
        ];

        await this.redis.set('names', JSON.stringify(names));

        for (const name of names) {
            const instance = new Test();
            instance.name = name;
            await this.testRepository.persist(instance);
        }
    }
}
